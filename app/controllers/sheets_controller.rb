class SheetsController < ApplicationController

  def index
    @sheets = Sheet.all
  end


  def new
    @sheet = Sheet.new
  end


  def create
    @sheet = Sheet.new(sheet_params)
    if @sheet.save 
      redirect_to @sheet
    else
      render 'new'
    end
  end


  def show
    @sheet = Sheet.find(params[:id])
  end


  def update
    @sheet = Sheet.find(params[:id]) 
    if @sheet.update(params[:sheet].permit!)
      redirect_to @sheet
    else
      render 'edit'
    end
  end


  def edit
    @sheet = Sheet.find(params[:id])
  end


  def destroy
    @sheet = Sheet.find(params[:id])
    @sheet.destroy
    redirect_to sheets_path
  end


  private
    def sheet_params
      params.require(:sheet).permit!
    end
end
