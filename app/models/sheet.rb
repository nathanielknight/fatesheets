class ApproachesValidator < ActiveModel::Validator

  def is_valid_approach_value?(x)
    x.is_a? Integer and x.between?(-4,10)
  end

  def validate(record)
    [:careful, :clever, :flashy, :forceful, :quick, :sneaky].each  do |a|
      x = record.send(a)
      unless x.is_a? Integer and x.between?(-4,10)
        record.errors[a] << "#{a.to_s.capitalize} should be a small integer, buddy."
      end
    end
  end
    
end

class Sheet < ActiveRecord::Base
  validates :name, presence: true
  validates_with ApproachesValidator
end
