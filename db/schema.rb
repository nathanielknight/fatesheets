# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131023140307) do

  create_table "sheets", force: true do |t|
    t.text     "name"
    t.text     "description"
    t.text     "high_concept"
    t.text     "trouble"
    t.text     "aspect1"
    t.text     "aspect2"
    t.text     "aspect3"
    t.integer  "careful"
    t.integer  "clever"
    t.integer  "flashy"
    t.integer  "forceful"
    t.integer  "quick"
    t.integer  "sneaky"
    t.boolean  "stress1"
    t.boolean  "stress2"
    t.boolean  "stress3"
    t.text     "consequence2"
    t.text     "consequence4"
    t.text     "consequence6"
    t.text     "stunts"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
