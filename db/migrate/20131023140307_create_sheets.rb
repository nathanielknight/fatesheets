class CreateSheets < ActiveRecord::Migration
  def change
    create_table :sheets do |t|
      t.text :name
      t.text :description
      t.text :high_concept
      t.text :trouble
      t.text :aspect1
      t.text :aspect2
      t.text :aspect3
      t.integer :careful
      t.integer :clever
      t.integer :flashy
      t.integer :forceful
      t.integer :quick
      t.integer :sneaky
      t.boolean :stress1
      t.boolean :stress2
      t.boolean :stress3
      t.text :consequence2
      t.text :consequence4
      t.text :consequence6
      t.text :stunts

      t.timestamps
    end
  end
end
