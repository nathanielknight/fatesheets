require 'test_helper'

class SheetControllerTest < ActionController::TestCase
  test "should get name:text" do
    get :name:text
    assert_response :success
  end

  test "should get description:text" do
    get :description:text
    assert_response :success
  end

  test "should get high_concept:text" do
    get :high_concept:text
    assert_response :success
  end

  test "should get trouble:text" do
    get :trouble:text
    assert_response :success
  end

  test "should get aspect1:text" do
    get :aspect1:text
    assert_response :success
  end

  test "should get aspect2:text" do
    get :aspect2:text
    assert_response :success
  end

  test "should get aspect3:text" do
    get :aspect3:text
    assert_response :success
  end

  test "should get careful:integer" do
    get :careful:integer
    assert_response :success
  end

  test "should get clever:integer" do
    get :clever:integer
    assert_response :success
  end

  test "should get flashy:integer" do
    get :flashy:integer
    assert_response :success
  end

  test "should get forceful:integer" do
    get :forceful:integer
    assert_response :success
  end

  test "should get quick:integer" do
    get :quick:integer
    assert_response :success
  end

  test "should get sneaky:integer" do
    get :sneaky:integer
    assert_response :success
  end

  test "should get stress1:bool" do
    get :stress1:bool
    assert_response :success
  end

  test "should get stress2:bool" do
    get :stress2:bool
    assert_response :success
  end

  test "should get stress3:bool" do
    get :stress3:bool
    assert_response :success
  end

  test "should get consequence2:text" do
    get :consequence2:text
    assert_response :success
  end

  test "should get consequence4:text" do
    get :consequence4:text
    assert_response :success
  end

  test "should get consequence6:text" do
    get :consequence6:text
    assert_response :success
  end

  test "should get stunts:text" do
    get :stunts:text
    assert_response :success
  end

end
